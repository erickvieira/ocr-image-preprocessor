package co.americashealth.ocrimageprocessor.models

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import lombok.AllArgsConstructor
import lombok.NoArgsConstructor

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
data class Image(
    @JsonProperty(value = "source", required = true)
    val source: String?,
    @JsonProperty(value = "contrast", defaultValue = "95", access = JsonProperty.Access.WRITE_ONLY)
    val contrast: Int = 95,
    @JsonProperty(value = "brightness", defaultValue = "50", access = JsonProperty.Access.WRITE_ONLY)
    val brightness: Int = 50,
    @JsonProperty(value = "fileName")
    val fileName: String? = null
)