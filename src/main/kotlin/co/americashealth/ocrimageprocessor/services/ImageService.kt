package co.americashealth.ocrimageprocessor.services

import co.americashealth.ocrimageprocessor.constants.getDefaultDir
import co.americashealth.ocrimageprocessor.constants.getSlash
import co.americashealth.ocrimageprocessor.models.Image
import co.americashealth.ocrimageprocessor.utils.sketchUp
import org.springframework.stereotype.Service
import java.io.File

@Service
class ImageService {

    fun sketchUp(image: Image) = File(image.run {
        val bc = arrayOf(0, 0)
        bc[0] = if (brightness != 0) brightness else 50
        bc[1] = if (contrast != 0) contrast else 95
        sketchUp(
                source!!,
                brightnessContrast = bc.first() to bc.last()
        )
    })

    fun getFile(fileName: String) = fileName.run {
        val fileAddress = getDefaultDir() + getSlash() + fileName
        File(fileAddress)
    }

}