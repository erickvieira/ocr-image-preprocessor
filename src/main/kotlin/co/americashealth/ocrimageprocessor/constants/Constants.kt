package co.americashealth.ocrimageprocessor.constants

import java.io.File

fun getHome() = System.getProperty("user.home")
fun getSlash() = File.separator
fun getDefaultDir() = getHome() + getSlash() + ".repository"