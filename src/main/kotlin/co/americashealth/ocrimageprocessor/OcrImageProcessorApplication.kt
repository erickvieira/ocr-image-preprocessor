package co.americashealth.ocrimageprocessor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OcrImageProcessorApplication

fun main(args: Array<String>) {
    runApplication<OcrImageProcessorApplication>(*args)
}
