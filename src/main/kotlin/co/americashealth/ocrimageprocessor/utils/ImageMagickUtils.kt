package co.americashealth.ocrimageprocessor.utils

import org.im4java.core.ConvertCmd
import org.im4java.core.Operation
import co.americashealth.ocrimageprocessor.constants.*

/**
 * @param source the source image to be sketched
 *
 * This method will sketch up an Image to highlight it`s details
 * by increasing contrast and brightness and adding greyscale
 */
fun sketchUp(source: String, brightnessContrast: Pair<Int, Int>): String {
    val convert = ConvertCmd()
    val op = Operation()
    with(op) {
        addImage()
        addRawArgs(
            "-brightness-contrast",
            "${brightnessContrast.first}X${brightnessContrast.second}",
            "-grayscale",
            "average"
        )
        addImage()
    }
    val now = System.currentTimeMillis()
    val fileName = "${"%x".format(now).toUpperCase()}.png"
    val filePath = getDefaultDir() + getSlash() + fileName
    convert.run(op, source, filePath)
    return filePath
}