package co.americashealth.ocrimageprocessor.controllers

import co.americashealth.ocrimageprocessor.models.Image
import co.americashealth.ocrimageprocessor.services.ImageService
import org.springframework.boot.configurationprocessor.json.JSONArray
import org.springframework.boot.configurationprocessor.json.JSONObject
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.nio.file.Files
import java.util.*

@RestController
@RequestMapping("/")
class ImageController(private val imageService: ImageService) {

    @GetMapping("/", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun hello() = ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
        JSONObject().run {
            this.put("version", "0.0.1")
            this.put("status", "running")
            this.put("available_methods", JSONArray(arrayOf(
                JSONObject(mutableMapOf(
                    "route" to "sketch",
                    "method" to "POST"
                )),
                JSONObject(mutableMapOf(
                    "route" to "file",
                    "method" to "GET"
                ))
            )))
            this.toString()
        }
    )

    /**
     * @param image the container for the source URL to be used
     * and the params to be passed for ImageMagick API
     * @param shouldEncode OPTIONAL - tells to the method that
     * the source of result Image must be a base64 string
     */
    @PostMapping("/sketch", produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun sketchUp(
        @RequestBody(required = true) image: Image,
        @RequestParam(defaultValue = "false") shouldEncode: Boolean
    ): ResponseEntity<Image> {
        val file = imageService.sketchUp(image)
        return ResponseEntity.ok().body(
            Image(
                if (shouldEncode) {
                    Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath()))
                } else null,
                fileName = file.name
            )
        )
    }

    /**
     * @param q - the file name to be displayed
     */
    @GetMapping("/file", produces = [MediaType.IMAGE_PNG_VALUE])
    fun getFile(
        @RequestParam(name = "q") fileName: String
    ): ResponseEntity<ByteArray> {
        val file = imageService.getFile(fileName)
        return ResponseEntity.ok().header(
            "Content-Disposition",
            "inline; filename=${file.name}"
        ).body(
            Files.readAllBytes(file.toPath())
        )
    }

}